class WeightController < ApplicationController
	
  def index
  port_str = 'COM1'
	baud_rate= 2400
	data_bits = 8
	stop_bits = 1
	parity = SerialPort::NONE

	sp= SerialPort.open(port_str,baud_rate,data_bits,stop_bits,parity)

	i=sp.gets.chomp
	sp.flush_input
	sp.close()
	
	@final_weight=i.gsub(/[^0-9]/,'').to_f

	@data={weight: @final_weight}

  end

  def pending_weighment
  	@urlstring="http://www.industryprime.com/grnitems/pending/weighment_pankaj.json"
  	@response=HTTParty.get(@urlstring)
    puts @response
  end


  def capture
    @captured_weight=""
  	@item_name=params[:grnitem_name]
  	@grnitem_id=params[:grnitem_radio]
    @type="final"


  	if (params[:commit]=="Capture")
      
    port_str = 'COM1'
		baud_rate= 9600
		data_bits = 8
		stop_bits = 1
		parity = SerialPort::NONE

		sp= SerialPort.open(port_str,baud_rate,data_bits,stop_bits,parity)

		i=sp.gets.chomp
		sp.flush_input
		sp.close()

		@final_weight=i.to_f.round(3)

		@captured_weight=@final_weight

    #@captured_weight=100.0
		
    end
    if (params[:commit]=="Submit")
      submitted_weight_string=params[:weight_value]
      @weight=submitted_weight_string.to_f

      	@urlstring="http://www.industryprime.com/grnitems/pending/weight_capture.json"
        HTTParty.post(@urlstring, :body => { "commit" => "Submit", "weight_type" => @type, "weight_value" => @weight, "grnitem_radio" => @grnitem_id })
      
      redirect_to url_for(:action => :pending_weighment)
    end


  end
end
