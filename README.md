This particular repository contains a project to capture weight from the weighing machine installed on the plant. This standalone app collects all system
data from the Industryprime web app where weighment is required. There is button on the home page to capture weight of particular item after clicking that
button the app will go on another page and will ask the user for weight capturing. This app operates upon rubygem 'serialport' to get the weight from weighment
machine via serial port. After getting the weight it will be stored to the webapp database via api call and this native app data will be updated accordingly.


For initial setup the following necessary things to be done.

1. Install ruby on rails on the native machine.
2. Run the system on native machine by executing bundle exec puma -w 0 command on production mode.
3. No database setup is required on native machine.

Before successful installation you need to need to install a desktop app named HyperTerminal on the native machine to get the information regarding serial 
port number and baud rate. After getting these information you need to replace the port number and baud rate in the weighment controller of this app.

