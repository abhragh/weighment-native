Rails.application.routes.draw do
  root 'weight#pending_weighment'
  get 'weight/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'weight/pending_weighment' => 'weight#pending_weighment'
  post 'weight/pending_weighment' => 'weight#pending_weighment'

  get 'weight/pending_weighment/capture' => 'weight#capture'
  post 'weight/pending_weighment/capture' => 'weight#capture'

  get '/capture' => 'weight#capture'
  post '/capture' => 'weight#capture'
end
